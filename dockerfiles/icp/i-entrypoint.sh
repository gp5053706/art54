#!/bin/bash

_makeDefaultDomainConfig(){
domain=$1
mkdir -p /static/$domain
cat<<EOF > /etc/nginx/sites-available/$domain
server {
        server_name   $domain *.$domain;

        access_log /var/log/nginx/$domain-access.log;
        error_log /var/log/nginx/$domain-error.log;

        location / {
                    proxy_pass http://trafik:80;
                    proxy_set_header Host            \$host;
                    proxy_set_header X-Forwarded-For \$remote_addr;
                    proxy_set_header X-Forwarded-Proto \$scheme;  
  		}
}
EOF

echo "[_makeDefaultDomainConfig] $domain end ..."
}

_reloadNginxConfig(){
	nginx -s reload
}

_initBefore(){
	echo "[/i-entrypoint.sh/_initBefore] start ..."
	file=/etc/nginx/sites-available/default
	if [ ! -f $file ]; then
		echo "[/i-entrypoint.sh/_initBefore] $file not found - cp from backup ..."
		cp -P /backup/$file $file
	fi
	file=/etc/nginx/sites-enabled/default
	if [ ! -f $file ]; then
		echo "[/i-entrypoint.sh/_initBefore] $file not found - cp from backup ..."
		cp -P /backup/$file $file
	fi	
	echo "[/i-entrypoint.sh/_initBefore] end ..."
}

_runCertBot(){
	cDomain=$1
	echo "[/i-entrypoint.sh/_runCertBot] start (domain:$cDomain) ..."
	if [ "x$cDomain" == "x" ]; then
		echo "[ERROR][_runCertBot] no domain name given."
		return 1
	fi
	cFile=/i.data/data/.certsEmail
	cEmail=$(cat $cFile)
	ret=$?
	if [ "$ret" != "0" ]; then
		echo "[ERROR][_runCertBot] problem reading file:$cFile (ret:$ret)"
		return $ret
	fi
	certbot certonly \
	--non-interactive --agree-tos --expand \
	-i nginx \
	--email $cEmail \
	--dns-ovh \
	--dns-ovh-credentials /run/secrets/certbot/certbot-ovh-api.ini \
	-d "$cDomain,*.$cDomain"
	ret=$?
	if [ "$ret" != "0" ]; then
		echo "[ERROR][_runCertBot] Problem registering certifacte for domain:$cDomain,*.$cDomain (ret:$ret)"
		return $ret
	fi

	echo "[/i-entrypoint.sh/_runCertBot] end ..."

	#TODO: https://certbot.org/renewal-setup
}

_loadDomains(){

	reloadConfNeeded=false
	domainsToCert=()
	point='[/i-entrypoint.sh/_loadDomains]'
	cd /domains
	for FILE in $(ls); do 
		echo "/domains/"$FILE;
		domain=$FILE
		wasT=false
		echo "$point check if domain $domain in sites-available ..."
		if [ -f /etc/nginx/sites-available/$domain ]; then
			echo "$point domain $domain present in sites-available"
		else # init domain config
			echo "$point domain $domain not found sites-available - make default config ..."
			_makeDefaultDomainConfig $domain
			wasT=true
		fi
		echo "$point check if domain $domain in sites-enabled ..."
		if [ -f /etc/nginx/sites-enabled/$domain ]; then
			echo "$point domain $domain present in sites-enabled"
		else
			echo "$point domain $domain not found sites-enabled - make link ..."
			ln -s /etc/nginx/sites-available/$domain /etc/nginx/sites-enabled/$domain
			#_reloadNginxConfig 
			#- no need - will be loaded ?
			wasT=true
		fi
		if [ "$reloadConfNeeded" = false ]; then
			reloadConfNeeded=$wasT
		fi
		if [ "$wasT" = true ]; then
			domainsToCert+=($domain)
		fi
	done
	
	if [ "$_reloadNginxConfig" = true ];then
		_reloadNginxConfig
	fi

	for domain in $domainsToCert; do 
		_runCertBot $domain
		ret=$?
		echo "[/i-entrypoint.sh/_loadDomains] _runCertBot $domain, ret:$ret"
	done

}

_initAfter(){
	cdir=$(pwd)
	echo "[/i-entrypoint.sh/_initAfter] start ($cdir)..."

	echo "[/i-entrypoint.sh/_initAfter] checking /domains ..."
	_loadDomains

	cd $cdir


	
	echo "[/i-entrypoint.sh/_init] end ..."
}

case "$1" in
	renew)
		echo "TODO!"
		;;
	loadDomains)
		_loadDomains
		;;
	reloadConfig)
		_reloadNginxConfig
		;;	
	startup)
		_initBefore
		doLoop=true
		while $doLoop; do
		nginx
		ret=$?
		if [ $ret -eq 0 ]; then
			echo "[/i-entrypoint.sh] nginx start ok"
			doLoop=false
		else
			echo "[/i-entrypoint.sh] nginx start failed - retry:$ret ..."
			#exit $ret
		fi
		done
		_initAfter
		echo "WARNING! check if init was done (/r init)"
		while true; do
			sleep 20
		done
		;;
	echo)
		echo "Echo from /i-entrypoint.sh: $@"
		;;
	help)
		echo "Usage:"	
		echo "======================================"
		;;
	*)
		#. ${I3C_HOME}/run.sh
		echo "[/i-entrypoint] '$1' Unimplemented."
		;;		
esac


#mkdir -p /etc/traefik/dynamic
: '
if [ ! -e /etc/traefik/traefik.yml ]; then
cat <<EOF > /etc/traefik/traefik.yml
## static configuration

global:
  # Send anonymous usage data
  sendAnonymousUsage: false

entryPoints:
  web:
    address: ":80"
#  websecure:
#    address: ":443"

# Access logs
# accessLog: fields:
#   headers:
#     names:
#       User-Agent: keep

providers:
  docker:
    endpoint: "unix:///var/run/docker.sock"
    exposedByDefault: false
  file:
    directory: /etc/traefik/dynamic
    watch: true

api:
  dashboard: true

log:
  level: ERROR
EOF
fi

/entrypoint.sh "$@"

'

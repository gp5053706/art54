

dParams="-d -p 80:80 -p 443:443 \
    -v $uData/domains:/domains \
    -v $uData/sites-enabled:/etc/nginx/sites-enabled \
    -v $uData/sites-available:/etc/nginx/sites-available \
    -v $uData/certs/letsencrypt:/etc/letsencrypt \
    -v $uLog/nginx:/var/log/nginx \
    -v $uLog/letsencrypt:/var/log/letsencrypt \
    -v /i3c/.secrets/certbot:/run/secrets/certbot \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    "

i3cAfter(){
    /i nc icp art54
    /i execd icp nginx -s reload
}
echo "i3c-build:$@ $(pwd)"
echo "cNameVariant:$cNameVariant"
echo "uSecrets:$uSecrets"
source $i3cScriptDir/.variantNames.sh

dbUser=$(/rsec dt/dbUser)
dbPass=$(/rsec dt/$cNameVariantFirstSeg.dbPass)
dbName=$(/rsec dt/dbName)
# default env - empty if not present
#if [ ! -e $i3cScriptDir/.env ]; then
#  touch $i3cScriptDir/.env
#fi
#cat < < EOF > $i3cScriptDir'/.env'$cNameVariant
mkdir -p $uSecrets
set -x
cat <<EOF > $uSecrets'/.env'
# generated from i3c-build
WP_PLUGINS="$(/rsec dt/wpPlugins)"
WP_URL="$(/rsec dt/$cNameVariant.wpUrl)" 
WP_USER=$(/rsec dt/wpUser) 
WP_PASSWORD=$(/rsec dt/$cNameVariantFirstSeg.wpPass) 
WP_EMAIL=$(/rsec dt/wpEmail) 
WP_TITLE="$(/rsec dt/$cNameVariant.wpTitle)"
WP_LOCALE=$(/rsec dt/wpLocale) 
WP_DESCRIPTION="$(/rsec dt/wpDesc)"
WP_DEBUG=$(/rsec dt/wpDebug)
WP_THEME=$(/rsec dt/wpTheme)
WORDPRESS_DB_HOST=$wcomDbContSanit:3306 
WORDPRESS_DB_USER=$dbUser  
WORDPRESS_DB_PASSWORD=$dbPass 
WORDPRESS_DB_NAME=$dbName 
EOF
ret=$?
if [ "$ret" != "0" ]; then
  echo "[i-build] Problem creating .env file:$ret"
  exit 1
fi



: '
dbUser=$(/rsec dt/dbUser)
dbPass=$(/rsec dt/dbPass)
dbName=$(/rsec dt/dbName)
dbRootPass=$(/rsec dt/dbRootPass) 

dParams="-p"
'

: '
cat <<EOF > docker-compose.yml
# generated from i3c-build

version: "3.9"
services:
  mariadb:
    image: mariadb
    environment:
      MYSQL_ROOT_PASSWORD: $dbRootPass
      MYSQL_DATABASE: $dbName
      MYSQL_USER: $dbUser
      MYSQL_PASSWORD: $dbPass      
    volumes:
      - $uData/database:/var/lib/mysql
  woocommerce:
    depends_on:
      - mariadb
    image: wordpress:latest
    volumes:
      - $uData/wordpress:/var/www/html
      - $uData/plugins:/var/www/html/wp-content/plugins
    ports:
      - "8082:80"
    restart: always
    environment:
      WORDPRESS_DB_HOST: mariadb:3306
      WORDPRESS_DB_USER: $dbUser
      WORDPRESS_DB_PASSWORD: $dbPass
      WORDPRESS_DB_NAME: $dbName
EOF
'
: '

  db:
    image: mysql:5.7
    volumes:
      - ./db_data:/var/lib/mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: somewordpress
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress
'
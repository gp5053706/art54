#!/bin/bash

# https://raw.githubusercontent.com/bozdoz/docker-wordpress-initialize/master/docker-install.sh
set -e
. /.env

sed -i "s/wordpress')/$WORDPRESS_DB_NAME')/g" wp-config.php
sed -i "s/example username')/$WORDPRESS_DB_USER')/g" wp-config.php
sed -i "s/example password')/$WORDPRESS_DB_PASSWORD')/g" wp-config.php
sed -i "s/mysql')/$WORDPRESS_DB_HOST')/g" wp-config.php

echo "[wcom-init] start checking ..."
if [ ! -f wp-config.php ]; then
    echo "WordPress not found in $PWD!"
    ( set -x; sleep 18 )
fi

if ! $(wp core is-installed); then
    echo "[wcom-init] Initializing WordPress install3!"
    echo "[wcom-init] WORDPRESS_DB_NAME:$WORDPRESS_DB_NAME"
    echo "[wcom-init] WORDPRESS_DB_HOST:$WORDPRESS_DB_HOST"
    #wp language core install $WP_LOCALE

    echo "[wcom-init] wp core install \
        --url="$WP_URL" \
        --admin_user=$WP_USER \
        --admin_password=$WP_PASSWORD \
        --admin_email=$WP_EMAIL \
        --title="$WP_TITLE" \
        --locale=$WP_LOCALE \
        --skip-email \
        --skip-plugins";
    
    

    wp core install \
        --url="$WP_URL" \
        --admin_user=$WP_USER \
        --admin_password=$WP_PASSWORD \
        --admin_email=$WP_EMAIL \
        --title="$WP_TITLE" \
        --locale=$WP_LOCALE \
        --skip-email \
        --skip-plugins

    wp language core install $WP_LOCALE
    wp language core activate $WP_LOCALE
    #wp core language install $WP_LOCALE
    #wp core install --locale=$WP_LOCALE

    wp option update timezone_string "Europe/Warsaw"
    wp option update time_format "H:i"

    echo "wpcore update"    
    wp core update
    
    wp core update-db

    wp option update blogdescription "$WP_DESCRIPTION"
    
    wp config set WP_DEBUG $WP_DEBUG --raw
    
    wp theme install $WP_THEME
    wp theme activate $WP_THEME
    
    wp rewrite structure '/%postname%/'
    
    wp plugin delete akismet hello

    if [ -n "$WP_PLUGINS" ]; then
        wp plugin install $WP_PLUGINS
        wp plugin activate --all
    fi
    
    # custom initial posts/pages script
    if [ -f /wcom/.onInit.sh ]; then
        . /wcom/.onInit.sh
    fi

    # make everything owned by www-data
    chown -R xfs:xfs . || true
fi

_checkRet(){
    echo "[_checkRet] $1 $2"    
    if [ "$1" != "0" ]; then
        echo "[ERROR][wc-updates][$FILE]:$2($1)"
        #export ret=$1
        exit $1
    fi
}

if [ -e /wc-updates ]; then
    cdir=$(pwd)
    echo "[wcom-init] /wc-updates present - checking for new files (cdir:$cdir)..."
    
    cd /wc-updates
    lso=$(ls)
    cd $cdir
    for FILE in $lso; do 
		echo "[wcom-init] $FILE";
        if [ ! -e /wcom/.wc-updates/$FILE ]; then
            mkdir -p /wcom/.wc-updates
            echo "[wcom-init] $FILE apply - start ..."
            set -o pipefail
            . /wc-updates/$FILE 2>&1 | tee /wcom/.wc-updates/$FILE.error
            ret=$?
            set +o pipefail
            #| ret=$? echo "rett:$ret" | tee $cdir/.wc-updates/$FILE.error
            #{ . /wc-updates/$FILE || echo "error:$?"; } 2>&1 | tee $cdir/.wc-updates/$FILE.error
            #ret=$?
            echo "[wcom-init] source/ret:$ret"
            if [ "$ret" != "0" ]; then
                echo "[ERROR] [wcom-init] source/ret:$ret"
                exit $ret
            fi
            mv /wcom/.wc-updates/$FILE.error /wcom/.wc-updates/$FILE.log
            cp /wc-updates/$FILE /wcom/.wc-updates/
            echo "[wcom-init] $FILE apply - end.";
        fi
    done
fi

if [ -f /wcom/.onStartup.sh ]; then
    . /wcom/.onStartup.sh
fi

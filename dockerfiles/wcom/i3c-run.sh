source $i3cScriptDir/.variantNames.sh

/i iup $wcomDbContName




#wpPlugins=$(/rsec dt/wpPlugins)

: '


'


echo "dparams variant:$cNameVariant"
echo "dparams variantFirstSeq:$cNameVariantFirstSeg"

#. $i3cScriptDir/.env$cNameVariant
. $uSecrets/.env

until docker container exec -it $wcomDbContSanit netstat -tulpn | grep LISTEN | grep "3306" ; do
  >&2 echo "Mariadb ($wcomDbContSanit:3306) is unavailable - waiting for it... 😴"
  sleep 1
done

dParams="-d `#-p 8000:80` \
    -e WP_URL="$WP_URL" \
    -e WP_USER=$WP_USER \
    -e WP_PASSWORD=$WP_PASSWORD \
    -e WP_EMAIL=$WP_EMAIL \
    -e WP_TITLE=\"$WP_TITLE\" \
    -e WP_LOCALE=$WP_LOCALE \
    -e WP_DESCRIPTION=\"$WP_DESCRIPTION\" \
    -e WP_DEBUG=$WP_DEBUG \
    -e WP_THEME=$WP_THEME \
    -e WORDPRESS_DB_HOST=$WORDPRESS_DB_HOST \
    -e WORDPRESS_DB_USER=$WORDPRESS_DB_USER \
    -e WORDPRESS_DB_PASSWORD=$WORDPRESS_DB_PASSWORD \
    -e WORDPRESS_DB_NAME=$WORDPRESS_DB_NAME \
    -v $uData/wcom:/wcom \
    -v $uData/wordpress:/var/www/html \
    -v $uData/plugins:/var/www/html/wp-content/plugins \
    "
#     -v $uData/.wp-cli:/var/www/.wp-cli \
#

i3cAfter(){
    echo "i3cAfter:$cNameSanit"
    /i nc $cNameSanit wcom$cNameVariant
    /i nc $cNameSanit art54
    #/i cp $i3cScriptDir'/.env'$cNameVariant $cNameSanit:/.env (ln w dockerfile)
    #/i cp $i3cScriptDir'/.env'$cNameVariant $cNameSanit:/.env
    #wait for wp run (TODO: better method?)
    sleep 10
    #/i exshd $cNameSanit "sudo -u www-data /wcom-init.sh"
    /i exshd $cNameSanit "/c init"
}
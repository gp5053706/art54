# https://doc.traefik.io/traefik/user-guides/docker-compose/basic-example/

# https://accesto.com/blog/docker-reverse-proxy-using-traefik/

dParams="-d `#-p 80:80` -p8080:8080 \
    -v $uData/etc/trafik:/etc/traefik \
    -v $uData/log:/var/log \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    "
#     -v $uData/etc/trafik/dynamic:/etc/traefik/dynamic \
#

rParams="
    --api.insecure=true `# allows accessing a Traefik dashboard - that simplifies debugging, but should be disabled outside of development environments due to security reasons` \
    --providers.docker=true `# enables the Docker configuration discovery` \
    --providers.docker.exposedbydefault=false `# do not expose Docker services by default` \
    --entrypoints.web.address=:80 `# create an entrypoint called web, listening on :80` \
    --metrics.prometheus=true `# https://stackoverflow.com/questions/58554731/traefik-v2-0-metrics-with-prometheus` \
    --metrics.prometheus.entryPoint=metrics \
    --entryPoints.metrics.address=:8082  \
    "

i3cAfter(){
    #/i execd trafik mkdir -p /etc/traefik/dynamic
    /i nc trafik art54
}


#  #- "--log.level=DEBUG" --configFile=foo/bar/myconfigfile.yml

# https://doc.traefik.io/traefik/getting-started/configuration-overview/echo

# ! https://github.com/korridor/reverse-proxy-docker-traefik
